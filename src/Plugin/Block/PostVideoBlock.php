<?php

namespace Drupal\social_post_video\Plugin\Block;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\social_post\Plugin\Block\PostBlock;

/**
 * Provides a 'PostvideoBlock' block.
 *
 * @Block(
 *  id = "post_video_block",
 *  admin_label = @Translation("Post video block"),
 * )
 */
class PostVideoBlock extends PostBlock {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $entityTypeManager, $currentUser, $formBuilder, ModuleHandler $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $currentUser, $formBuilder, $moduleHandler);
    // Override the bundle type.
    $this->bundle = 'video';
  }

}
