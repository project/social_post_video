<?php

namespace Drupal\social_post_video\Plugin\Block;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\social_post\Plugin\Block\PostProfileBlock;

/**
 * Provides a 'PostvideoProfileBlock' block.
 *
 * @Block(
 *  id = "post_video_profile_block",
 *  admin_label = @Translation("Post video on profile of others block"),
 * )
 */
class PostVideoProfileBlock extends PostProfileBlock {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $entityTypeManager, $currentUser, $formBuilder, ModuleHandler $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $currentUser, $formBuilder, $moduleHandler);
    // Override the bundle type.
    $this->bundle = 'video';
  }

}
