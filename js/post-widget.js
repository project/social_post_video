/**
 * @file
 * extends the image widget width eventlisteners and triggers for customized presentation.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.postvideoWidget = {
    attach: function (context, settings) {

      $(document, context).once('field-post-image-add').on('click', '#post-video-add', function(e) {
        $('input[data-drupal-selector="edit-field-post-image-0-upload"]').trigger('click');
        e.preventDefault();
      });

      $(document, context).once('field-post-image-remove').on('click', '#post-video-remove', function(e) {
        $('button[data-drupal-selector="edit-field-post-image-0-remove-button"]').trigger('mousedown');
        e.preventDefault();
      });

      // Change placeholder text when someone adds a video.
      $('[data-drupal-selector="edit-field-post-image-0-upload"]').change(function(e) {
        $('#edit-field-post-0-value').attr("placeholder", Drupal.t('Say something about this video'));
        $('[data-drupal-selector="edit-field-post-image-wrapper"] .spinner').remove();
        $('[data-drupal-selector="edit-field-post-image-wrapper"] .form-group .form-group').prepend('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
      });

    }
  };
})(jQuery);
